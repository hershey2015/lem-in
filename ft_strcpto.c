/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpto.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 17:21:21 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 08:14:57 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

char		*ft_strcpto(const char *s1, char c)
{
	char	*str1;
	int		i;

	i = 0;
	str1 = malloc(sizeof(char) * ft_strlen(s1) + 1);
	if (!str1)
		return (0);
	while (s1[i] != c && s1[i] != '\0')
	{
		str1[i] = s1[i];
		i++;
	}
	str1[i] = '\0';
	return (str1);
}
