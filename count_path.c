/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count_path.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 15:25:38 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 16:44:37 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int		count(int k, int ant, t_path *path, int pa)
{
	int		i;
	t_path	*pat;
	int		dell;

	k += ant;
	dell = k % pa;
	k = k - dell;
	k /= pa;
	i = 0;
	pat = path;
	while (i < pa)
	{
		path->ants = k - path->longe;
		i++;
		path = path->next;
	}
	i = 0;
	path = pat;
	while (i < pa && dell)
	{
		path->ants++;
		dell--;
		path = path->next;
	}
	return (0);
}

int		count_pa(int ant, t_path *path, int *pa)
{
	int k;
	int i;

	i = 1;
	k = 0;
	while (path->next->next != NULL && k < ant)
	{
		k += ((path->next->longe - path->longe) * i);
		i++;
		path = path->next;
		(*pa)++;
	}
	if (k < ant)
		(*pa)++;
	return (0);
}

int		count_path(t_path *path, int ant, int *err)
{
	t_path		*pat;
	int			pa;
	int			k;
	int			i;

	pa = 0;
	pat = path;
	count_pa(ant, path, &pa);
	k = 0;
	i = 0;
	path = pat;
	while (i < pa)
	{
		k += pat->longe;
		i++;
		pat = pat->next;
	}
	count(k, ant, path, pa);
	return (0);
}
