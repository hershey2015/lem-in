/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 16:58:12 by coleksii          #+#    #+#             */
/*   Updated: 2016/12/15 16:35:31 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int c, int fd)
{
	unsigned int	n;
	char			t;

	n = c;
	if (c < 0)
	{
		ft_putchar_fd('-', fd);
		n = -c;
	}
	else
		n = c;
	if (n <= 9)
	{
		t = n + '0';
		write(fd, &t, 1);
	}
	else
	{
		ft_putnbr_fd(n / 10, fd);
		ft_putnbr_fd(n % 10, fd);
	}
}
