/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   head.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/07 14:21:56 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 22:08:20 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEAD_H
# define HEAD_H

# include <fcntl.h>
# include <stdlib.h>
# include <unistd.h>
# include "libft/libft.h"
# include "printf/ft_printf.h"
# include "get_next_line.h"

typedef	struct	s_l
{
	char		*name;
	int			id;
	int			numb;
	struct s_l	*next;
	void		*link;
}				t_com;
typedef struct	s_lo
{
	char		*name;
	int			numb;
	t_com		*com;
	struct s_lo	*next;
	struct s_lo	*link;
	int			x;
	int			y;
	int			id;
}				t_real;
typedef	struct	s_pa
{
	struct s_pa	*next;
	struct s_pa	*prev;
	char		**path;
	char		**may;
	int			longe;
	int			ants;
	int			ants2;
	int			index;
	int			*num;
}				t_path;
int				get_next_line(const int fd, char **line);
char			*ft_stradd(char **s1, char *s2);
int				ft_isnumb(char *s, char c);
int				analiz(t_real *beg, char **str, int *err, char **s);
int				get_value(char *s, t_real *list);
char			*ft_strcpto(const char *s1, char c);
int				thinking(t_real *beg, int ant, int *err, char *str);
int				create_comm(char *s, t_real *beg, int *err);
int				printing(t_real *beg);
int				count_path(t_path *path, int ant, int *err);
int				print_path(t_path *path, int ant, int *err);
int				clear(t_real *beg, t_path *path, int *err);
int				clear_room(t_real *beg);
void			paths(t_path *path);
int				creator(t_path **pat, int *err);
int				print_all(t_path *path, int ant, int *err, t_real *beg);
int				check_begend(t_real *beg, t_path *path, int *err);
int				recursion(t_path *path, t_com *com, int *err, int i);
int				set_name(char **path, char *name, int i);
int				startend(t_real *beg, int *err, char **str, char **s);

#endif
