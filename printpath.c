/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printpath.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 15:58:59 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 21:19:03 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

void	paths(t_path *path)
{
	int	i;

	i = 0;
	while (path->next != NULL)
	{
		ft_printf("\npath = ");
		i = 0;
		while (path->path[i] != NULL)
		{
			ft_printf("\033[34m%s \033[0m", path->path[i]);
			++i;
		}
		ft_printf("\nlength = %d", path->longe);
		ft_printf("\n");
		path = path->next;
	}
}

int		norma(int *ant, t_path *buf, int i)
{
	int j;
	int p;

	j = i;
	p = 1;
	while (j >= 0)
	{
		if (j <= buf->ants2 - 1 && p <= buf->longe)
		{
			ft_printf("L%d-%s ", buf->num[buf->ants2 - 1 - j], buf->path[p]);
			if (p >= buf->longe)
				(*ant)--;
		}
		j--;
		p++;
	}
	return (0);
}

int		conte(int ant, t_path *path)
{
	t_path	*buf;
	int		j;
	int		p;
	int		i;

	i = 0;
	while (ant)
	{
		buf = path;
		while (buf->next != NULL && buf->ants2)
		{
			norma(&ant, buf, i);
			buf = buf->next;
		}
		ft_printf("\n");
		i++;
	}
	return (0);
}

int		numeric(int ant, t_path *path)
{
	t_path	*buf;
	int		ants;
	int		i;

	i = 0;
	ants = ant;
	while (ants)
	{
		buf = path;
		while (buf->next != NULL)
		{
			if (buf->ants)
			{
				buf->num[buf->ants - 1] = i + 1;
				ants--;
				i++;
				buf->ants--;
			}
			buf = buf->next;
		}
	}
	return (0);
}

int		print_path(t_path *path, int ant, int *err)
{
	t_path *buf;

	buf = path;
	err[8] = 1;
	while (buf->next != NULL && buf->ants)
	{
		buf->num = (int *)malloc(sizeof(int) * buf->ants);
		buf->ants2 = buf->ants;
		buf->index = 0;
		buf = buf->next;
	}
	numeric(ant, path);
	conte(ant, path);
	if (err[7])
		paths(path);
	return (0);
}
