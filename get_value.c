/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_value.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/21 16:04:50 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 14:32:25 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int	get_value(char *s, t_real *list)
{
	int	i;

	i = 0;
	list->name = ft_strcpto(s, ' ');
	list->numb = ft_atoi(s);
	if (*s == 'L')
		return (0);
	while (s[i] != ' ' && s[i] != '\0')
		i++;
	while (s[i] == ' ' && s[i] != '\0')
		i++;
	if (!ft_isnumb(&s[i], ' '))
		return (0);
	list->x = ft_atoi(&s[i]);
	while ((s[i] > 47 && s[i] < 58) || s[i] == '-')
		i++;
	while (s[i] == ' ')
		i++;
	if (!ft_isnumb(&s[i], '\0'))
		return (0);
	list->y = ft_atoi(&s[i]);
	return (1);
}

int	clear(t_real *beg, t_path *path, int *err)
{
	t_path	*buf;

	while (path != NULL)
	{
		free(path->may);
		free(path->path);
		if (err[8] && path->ants)
			free(path->num);
		buf = path->next;
		free(path);
		path = buf;
	}
	return (0);
}

int	clear_room(t_real *beg)
{
	t_real	*buf;
	t_com	*com;
	t_com	*irr;

	while (beg != NULL)
	{
		com = beg->com;
		while (com != NULL)
		{
			irr = com->next;
			free(com->name);
			free(com);
			com = irr;
		}
		buf = beg->next;
		free(beg->name);
		free(beg);
		beg = buf;
	}
	return (0);
}
