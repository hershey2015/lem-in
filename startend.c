/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   startend.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 22:04:47 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 22:05:54 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

t_real	*beglast(t_real *beg)
{
	t_real	*one;

	one = beg;
	while (one->next != NULL)
	{
		one = one->next;
	}
	free(one->name);
	return (one);
}

int		end(t_real *beg, int *err, char **str, char **s)
{
	if (err[3] && (err[0] = 1))
		return (0);
	ft_stradd(str, *s);
	get_next_line(0, s);
	while (**s == '#' && ft_strcmp(*s, "##start") && ft_strcmp(*s, "##end"))
	{
		ft_stradd(str, *s);
		get_next_line(0, s);
	}
	if (**s == '#' && (!ft_strcmp(*s, "##start") || !ft_strcmp(*s, "##end")))
	{
		*err = 1;
		return (0);
	}
	err[3] = 1;
	if (!get_value(*s, beglast(beg)))
	{
		*err = 1;
		return (0);
	}
	return (1);
}

int		start(t_real *beg, int *err, char **str, char **s)
{
	if (err[2] && (err[0] = 1))
		return (0);
	ft_stradd(str, *s);
	if (!get_next_line(0, s) && (err[0] = 1))
		return (0);
	free(beg->name);
	while (**s == '#' && ft_strcmp(*s, "##start") && ft_strcmp(*s, "##end"))
	{
		ft_stradd(str, *s);
		get_next_line(0, s);
	}
	if (**s == '#' && (!ft_strcmp(*s, "##start") || !ft_strcmp(*s, "##end")))
	{
		*err = 1;
		return (0);
	}
	err[2] = 1;
	if (!get_value(*s, beg))
	{
		*err = 1;
		return (0);
	}
	return (1);
}

int		startend(t_real *beg, int *err, char **str, char **s)
{
	if (!ft_strcmp(*s, "##start"))
	{
		if (!start(beg, err, str, s))
			return (0);
	}
	else if (!ft_strcmp(*s, "##end"))
		if (!end(beg, err, str, s))
			return (0);
	return (2);
}
