/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_com.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/21 16:32:25 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 15:39:09 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int		power(t_real **beg, char *name2, t_com **com, int *err)
{
	while (!ft_strequ((*beg)->name, name2) && (*beg)->next != NULL)
		(*beg) = (*beg)->next;
	if (!ft_strequ((*beg)->name, name2) && (err[0] = 1))
		return (0);
	(*com) = (t_com *)malloc(sizeof(t_com));
	(*com)->next = NULL;
	(*com)->name = name2;
	return (1);
}

int		comer(t_real *beg, t_com *com)
{
	com->id = beg->id;
	com->link = beg;
	return (0);
}

int		cre_mem(char *name2, t_real *buf, int *err, t_real *beg)
{
	t_com	*com;
	t_com	*c;

	if (!(power(&beg, name2, &com, err)))
		return (0);
	if (buf->com == NULL && (buf->com = com))
		comer(beg, com);
	else
	{
		c = buf->com;
		while (c->next != NULL)
		{
			if (ft_strequ(c->name, name2))
				return (0);
			c = c->next;
		}
		if (ft_strequ(c->name, name2))
			return (0);
		c->next = com;
		com->id = beg->id;
		com->link = beg;
	}
	return (0);
}

int		create_comm(char *s, t_real *beg, int *err)
{
	int		i;
	int		j;
	char	*name;
	char	*name2;
	t_real	*buf;

	buf = beg;
	name = ft_strcpto(s, '-');
	j = 0;
	while (s[j] != '-' && s[j] != '\0')
		++j;
	++j;
	name2 = ft_strcpto(&s[j], '\0');
	while (!ft_strequ(buf->name, name) && buf->next != NULL)
		buf = buf->next;
	if (!ft_strequ(buf->name, name) && (*err = 1))
		return (0);
	cre_mem(name2, buf, err, beg);
	buf = beg;
	while (!ft_strequ(buf->name, name2) && buf->next != NULL)
		buf = buf->next;
	cre_mem(name, buf, err, beg);
	return (0);
}
