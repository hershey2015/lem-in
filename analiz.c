/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   analiz.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/14 18:34:32 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 22:09:27 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int		ft_withoutspace(char *s, char c)
{
	while (*s != c)
	{
		if (*s == ' ')
			return (0);
		++s;
	}
	return (1);
}

int		check(char *s, int *err)
{
	int i;

	i = 0;
	while (s[i] != ' ' && s[i] != '-' && s[i] != '\0')
		++i;
	if (s[i] == '-')
	{
		if (ft_withoutspace(&s[++i], '\0'))
			return (2);
	}
	else if (s[i] == ' ')
	{
		if (ft_isnumb(&s[++i], ' '))
		{
			while ((s[i] > 47 && s[i] < 58) || s[i] == '-')
				++i;
			if (s[i] == ' ' && ft_isnumb(&s[++i], '\0'))
				return (1);
		}
	}
	*err = 1;
	return (0);
}

int		antiplagiat(t_real *beg, t_real *new, int *err)
{
	t_real	*buf;

	buf = beg;
	while (buf->next != NULL)
	{
		if ((ft_strequ(buf->name, new->name) ||
					(buf->x == new->x && buf->y == new->y)))
		{
			err[0] = 1;
			return (0);
		}
		buf = buf->next;
	}
	return (0);
}

int		creat_list(char *s, t_real *beg, int *err)
{
	t_real *list;
	t_real *new;
	t_real *buf;

	list = beg;
	while (list->next != NULL)
	{
		buf = list;
		list = list->next;
	}
	new = (t_real *)malloc(sizeof(t_real));
	new->com = NULL;
	new->id = err[5]++;
	if (!get_value(s, new))
		err[0] = 1;
	antiplagiat(beg, new, err);
	buf->next = new;
	new->next = list;
	return (0);
}

int		analiz(t_real *beg, char **str, int *err, char **s)
{
	int		ind;
	int		i;

	ind = 0;
	i = 0;
	if (**s == '#' && (i = startend(beg, err, str, s)))
	{
		if (i == 2)
			return (2);
		return (0);
	}
	if (((ind = check(*s, err)) == 2))
	{
		create_comm(*s, beg, err);
		err[1] = 1;
		return (1);
	}
	if (ind == 1)
	{
		err[4]++;
		creat_list(*s, beg, err);
	}
	return (0);
}
