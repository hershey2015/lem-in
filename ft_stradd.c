/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stradd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/11 20:00:18 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/18 16:06:05 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

char	*ft_stradd(char **s1, char *s2)
{
	char	*str;
	int		i;
	int		j;
	char	*s;

	i = 0;
	j = 0;
	s = *s1;
	str = (char *)malloc(ft_strlen(*s1) + ft_strlen(s2) + 2);
	while (s1[0][i] != '\0')
	{
		str[i] = s1[0][i];
		++i;
	}
	str[i++] = '\n';
	while (s2[j] != '\0')
		str[i++] = s2[j++];
	str[i] = '\0';
	*s1 = str;
	free(s);
	free(s2);
	return (str);
}
