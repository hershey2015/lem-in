/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem-in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/11 17:09:18 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 10:12:22 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

t_real	*begin_create(void)
{
	t_real	*adr;
	t_real	*end;

	adr = (t_real *)malloc(sizeof(t_real));
	end = (t_real *)malloc(sizeof(t_real));
	adr->next = end;
	end->next = NULL;
	adr->com = NULL;
	end->com = NULL;
	adr->id = -1;
	end->id = -2;
	adr->name = ft_strdup("");
	end->name = ft_strdup("");
	return (adr);
}

int		ant(char *s, char **str)
{
	int	ant;
	int	i;

	while (*s == '#' && (!ft_strequ("##start", s) && !ft_strequ("##end", s)))
	{
		ft_stradd(str, s);
		get_next_line(0, &s);
	}
	if (*s == '#' && (ft_strequ("##start", s) && ft_strequ("##end", s)))
		return (0);
	i = 0;
	if (*s < 48 || *s > 57)
	{
		ft_stradd(str, s);
		return (0);
	}
	ant = ft_atoi(s);
	while (s[i] > 47 && s[i] < 58)
		++i;
	ft_stradd(str, s);
	if (s[i] != '\0')
		return (0);
	return (ant);
}

/* ************************************************************************** */
/* err[0] - индикатор ошибки 												  */
/* err[1] - инбикатор того что бы после связей не начались комнаты
 * err[2] - старт
 * err[3] - end		  */
/* err[4] - количество комнат
 * err[5] - индивидуальны номер комнаты*/
/* err[6] - индикатор что старт связан с ендом */
/* err[7] - индикатор флага -с для графония		*/
/* err[8] - заходит оно в вывод или нет			*/
/* analiz должен возвращать 0 если комнта и 1 если связь комнат				  */
/* ************************************************************************** */

int		so(int *err, int ants, t_real *beg, char *str)
{
	if (!err[2] || !err[3])
		err[0] = 1;
	if (!err[0])
		thinking(beg, ants, err, str);
	free(str);
	if (err[0] && write(1, "ERROR\n", 6))
		return (0);
	if (err[7])
		printing(beg);
	return (0);
}

int		prepare(int *err, char **str, t_real **beg)
{
	err[0] = 1;
	err[1] = 0;
	err[2] = 0;
	err[3] = 0;
	err[4] = 2;
	err[5] = 0;
	err[6] = 0;
	err[7] = 0;
	err[8] = 0;
	*beg = begin_create();
	*str = (char *)malloc(1);
	**str = '\0';
	return (0);
}

int		main(int ar, char **gv)
{
	char	*s;
	char	*str;
	int		err[9];
	int		ants;
	t_real	*beg;

	prepare(err, &str, &beg);
	if (ar > 0 && ft_strequ(gv[1], "-c"))
		err[7] = 1;
	if (get_next_line(0, &s) && (((ants = ant(s, &str)) > 0)))
		err[0] = 0;
	while ((get_next_line(0, &s))/*&& !ft_strequ(s, "\0")*/)
	{
		if (!analiz(beg, &str, err, &s) && err[1])
			err[0] = 1;
		if (err[0] && write(1, "ERROR\n", 6))//очищение от ликов
			return (0);
		ft_stradd(&str, s);
	}
	so(err, ants, beg, str);
	clear_room(beg);
	return (0);
}
