# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: coleksii <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/08/22 17:14:24 by coleksii          #+#    #+#              #
#    Updated: 2017/08/22 22:10:05 by coleksii         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = @gcc

NAME = lem-in

LIB = libft.a

LIBPRI = libftprintf.a

HEAD = head.h

#----	path	----
LIB_PATH = ./libft
PRINTF_PATH = ./printf
OBJ_PATH = /objective

#----	files	----
FILES_NAME = lemin get_next_line ft_stradd ft_isnumber \
		ft_strcpto analiz think create_com printing \
		count_path printpath get_value recurs startend

#----	other	----
FILES = $(addsuffix .c, $(FILES_NAME))
FILES_OBJ = $(addsuffix .o, $(FILES_NAME))

#----	make	----

all: $(NAME)

$(NAME): $(FILES_OBJ)
	@make -C libft/
	@make -C printf/
	@gcc -o $@ $^ libft/libft.a printf/libftprintf.a
	@echo "\033[32;1m<<lem-in done>>\033[0m"

clean:
	@make clean -C libft/
	@make clean -C printf/
	@rm -rf $(addsuffix .o, $(FILES_NAME))

fclean:
	@make fclean -C libft/
	@make fclean -C printf/
	@rm -rf $(addsuffix .o, $(FILES_NAME))
	@rm -rf $(NAME)

re: fclean all
	@echo "\033[32;1m<<re lemin succes>>\033[0m"
