/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printing.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/01 14:06:28 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 21:40:31 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

void	fight(t_real *beg, t_real *buf, t_com *com)
{
	ft_printf("\033[32m[%5s]\033[0m\033[43;30m%3d\033[0m\t",
			beg->name, beg->id);
	com = beg->com;
	while (com && com->next != NULL)
	{
		ft_printf("- \033[35m{%5s}\033[0m\033[43;30m%3d\033[0m\t",
				com->name, com->id);
		com = com->next;
	}
	if (beg->com != NULL)
	{
		buf = (t_real *)com->link;
		ft_printf("- \033[35m{%5s}\033[0m\033[43;30m%3d\033[0m\t",
				com->name, com->id);
	}
	ft_printf("\n");
}

int		printing(t_real *beg)
{
	t_com	*com;
	t_real	*buf;

	ft_printf("\n");
	while (beg->next != NULL)
	{
		com = beg->com;
		ft_printf("\033[32m[%5s]\033[0m\033[43;30m%3d\033[0m\t", beg->name,
				beg->id);
		while (com && com->next)
		{
			buf = (t_real *)com->link;
			ft_printf("- \033[35m{%5s}\033[0m\033[43;30m%3d\033[0m\t",
					com->name, com->id);
			com = com->next;
		}
		if (beg->com != NULL && (buf = (t_real *)com->link))
			ft_printf("- \033[35m{%5s}\033[0m\033[43;30m%3d\033[0m\t",
					com->name, com->id);
		ft_printf("\n");
		beg = beg->next;
	}
	fight(beg, buf, com);
	return (0);
}

int		creator(t_path **pat, int *err)
{
	(*pat) = (t_path *)malloc(sizeof(t_path));
	(*pat)->next = NULL;
	(*pat)->prev = NULL;
	(*pat)->path = (char **)malloc(sizeof(char *) * (err[4] + 1));
	(*pat)->may = (char **)malloc(sizeof(char *) * (err[4] + 1));
	(*pat)->ants = 0;
	(*pat)->longe = err[4] + 1;
	return (0);
}

int		print_all(t_path *path, int ant, int *err, t_real *beg)
{
	int i;

	i = 1;
	while (i <= ant)
	{
		ft_printf("L%d-%s ", i, path->path[0]);
		i++;
	}
	ft_printf("\n");
	if (err[7])
		ft_printf("\npath =\033[34m %s %s\033[0m\nlength = 1\n",
				beg->name, beg->next->name);
	return (0);
}

int		check_begend(t_real *beg, t_path *path, int *err)
{
	t_com	*buf;

	buf = beg->com;
	while (buf != NULL)
	{
		if (buf->id == -2)
		{
			path->path[0] = buf->name;
			err[6] = 1;
			return (1);
		}
		buf = buf->next;
	}
	return (0);
}
