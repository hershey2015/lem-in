/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recurs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 21:41:24 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 22:02:33 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int		room_finder(t_path *pat, char *name, int p, int i)
{
	int		j;
	int		l;
	t_path	*path;

	path = pat;
	j = 0;
	while (path->may[j] != NULL)
		if (ft_strequ(path->may[j++], name))
			return (1);
	if (path->prev != NULL && (path = path->prev))
		while (path != NULL)
		{
			l = 1;
			while (path->path[l] != NULL)
			{
				if (ft_strequ(path->path[l], name))
					return (1);
				++l;
			}
			path = path->prev;
		}
	if (p == 1)
		pat->may[j] = name;
	return (0);
}

int		null_name(char **path, char *name, int i)
{
	while (path[i] != NULL)
	{
		path[i] = NULL;
		i++;
	}
	return (0);
}

int		null_name2(char **path, char *name, int i)
{
	path[i] = NULL;
	return (0);
}

int		ti_fi(t_path *path, t_com *com, int i)
{
	if (path->longe > i)
	{
		null_name(path->may, com->name, i);
		null_name(path->path, com->name, i);
		path->longe = i;
		return (i);
	}
	else
		return (0);
}

int		recursion(t_path *path, t_com *com, int *err, int i)
{
	t_real	*real;
	int		max;
	int		min;

	max = 0;
	min = 0;
	while (com != NULL)
	{
		if (com->id == -2)
			return (ti_fi(path, com, i));
		if (!(room_finder(path, com->name, 1, i)))
		{
			real = (t_real *)com->link;
			if (((max = recursion(path, real->com, err, i + 1)) != 0))
			{
				min = max;
				set_name(path->path, com->name, i);
				null_name2(path->may, com->name, i);
			}
			else
				null_name(path->may, com->name, i);
		}
		com = com->next;
	}
	return (min);
}
