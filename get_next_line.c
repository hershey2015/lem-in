/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 13:04:17 by coleksii          #+#    #+#             */
/*   Updated: 2017/07/07 17:11:07 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

char	*ft_join(char *s1, char *s2)
{
	char	*str;
	size_t	i;

	i = 0;
	if (!s1)
		i = 0;
	else
		while (s1[i] != '\0')
			i++;
	str = (char *)malloc((i + BUFF_SIZE + 1));
	if (!str)
		return (0);
	i = 0;
	while (s1 && *s1 != '\0')
		str[i++] = *s1++;
	while (s2 && *s2 != '\0')
		str[i++] = *s2++;
	str[i] = '\0';
	return (str);
}

t_list	*ft_lstcmp(int fd, t_list *lst)
{
	while (lst)
	{
		if (fd == (int)lst->content_size)
			return (lst);
		lst = lst->next;
	}
	return (NULL);
}

int		lol(char *s)
{
	int i;

	i = ft_strlen(s);
	if (i == 0 || (i == 1 && s[0] == '\n'))
		return (0);
	return (1);
}

int		lal(char **k, t_list *lst, int fd)
{
	int		n;
	char	*buf;
	char	*cry;

	if (fd == 1)
		return (-1);
	n = 2;
	*k = NULL;
	buf = (char *)malloc(BUFF_SIZE + 1);
	while (n > 0)
	{
		if (lst->content && (*k = ft_strchr(lst->content, '\n')))
		{
			**k = '\0';
			(*k)++;
			break ;
		}
		n = read(fd, buf, BUFF_SIZE);
		buf[n] = '\0';
		cry = lst->content;
		lst->content = ft_join(lst->content, buf);
		free(cry);
	}
	free(buf);
	return (n);
}

int		get_next_line(const int fd, char **line)
{
	static t_list	*lst;
	t_list			*begin;
	int				n;
	char			*k;

	if (!lst)
		lst = ft_lstnew("", fd);
	begin = lst;
	if (!(ft_lstcmp(fd, lst)))
	{
		ft_lstadd(&lst, ft_lstnew("", 0));
		lst->content_size = fd;
		begin = lst;
	}
	else
		lst = ft_lstcmp(fd, lst);
	n = lal(&k, lst, fd);
	if (n == 0 && !lol(lst->content))
		return (0);
	if (n == -1)
		return (n);
	*line = (char *)lst->content;
	lst->content = (k) ? (ft_strdup(k)) : NULL;
	lst = begin;
	return (1);
}
