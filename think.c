/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   think.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/20 16:20:05 by coleksii          #+#    #+#             */
/*   Updated: 2017/08/22 22:01:54 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "head.h"

int		cre_path(t_path **path, t_real *beg, int *err)
{
	int	i;

	(*path)->next = (t_path *)malloc(sizeof(t_path));
	(*path)->next->prev = *path;
	(*path) = (*path)->next;
	(*path)->next = NULL;
	(*path)->longe = err[4] + 1;
	(*path)->path = (char **)malloc(sizeof(char *) * (err[4] + 1));
	(*path)->may = (char **)malloc(sizeof(char *) * (err[4] + 1));
	(*path)->ants = 0;
	i = 0;
	while (i <= (err[4]))
	{
		(*path)->may[i] = NULL;
		(*path)->path[i++] = NULL;
	}
	i = 1;
	(*path)->path[0] = beg->name;
	(*path)->may[0] = beg->name;
	return (0);
}

int		set_name(char **path, char *name, int i)
{
	path[i] = name;
	return (0);
}

int		find_ind_path(t_real *beg, int ant, int *err, t_path *path)
{
	t_real	*real;
	t_com	*com;
	int		i;

	i = 1;
	real = beg;
	com = real->com;
	path->path[0] = beg->name;
	path->longe = err[4] + 1;
	while ((recursion(path, com, err, i)))
		cre_path(&path, beg, err);
	if (path->prev == NULL)
		err[0] = 1;
	while (beg->next != NULL)
		beg = beg->next;
	path = path->prev;
	while (path != NULL)
	{
		i = 0;
		while (path->path[i] != NULL)
			++i;
		path->path[i] = beg->name;
		path = path->prev;
	}
	return (0);
}

int		thinking(t_real *beg, int ant, int *err, char *str)
{
	int		i;
	t_path	*pat;

	i = 0;
	creator(&pat, err);
	while (i <= (err[4]))
	{
		pat->may[i] = NULL;
		pat->path[i++] = NULL;
	}
	if (!check_begend(beg, pat, err))
		find_ind_path(beg, ant, err, pat);
	if (err[0])
		return (0);
	write(1, str + 1, ft_strlen(str));
	write(1, "\n\n", 2);
	if (!err[6])
	{
		count_path(pat, ant, err);
		print_path(pat, ant, err);
	}
	else
		print_all(pat, ant, err, beg);
	clear(beg, pat, err);
	return (0);
}
